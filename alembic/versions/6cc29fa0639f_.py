"""empty message

Revision ID: 6cc29fa0639f
Revises: 4f00c7bc45d3
Create Date: 2022-04-20 08:17:02.668879

"""
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from alembic import op

# revision identifiers, used by Alembic.
revision = "6cc29fa0639f"
down_revision = "4f00c7bc45d3"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "user",
        sa.Column("id", postgresql.UUID(as_uuid=True), nullable=False),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=False),
        sa.Column("updated_at", sa.DateTime(timezone=True), nullable=False),
        sa.Column("first_name", sa.Text(), nullable=False),
        sa.Column("last_name", sa.Text(), nullable=False),
        sa.Column("email", sa.Text(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_user_email"), "user", ["email"], unique=False)

    boat = postgresql.ENUM("OPTIMIST", "LASER47", name="boat")
    boat.create(op.get_bind())
    op.add_column(
        "competition",
        sa.Column("boat", sa.Enum("OPTIMIST", "LASER47", name="boat"), nullable=False),
    )

    role = postgresql.ENUM("ORGANISOR", "OFFICIAL", "COMPETITOR", name="role")
    role.create(op.get_bind())
    op.add_column(
        "usercompetition",
        sa.Column(
            "role",
            sa.Enum("ORGANISOR", "OFFICIAL", "COMPETITOR", name="role"),
            nullable=False,
        ),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("usercompetition", "role")
    role = postgresql.ENUM("ORGANISOR", "OFFICIAL", "COMPETITOR", name="role")
    role.drop(op.get_bind())

    op.drop_column("competition", "boat")
    boat = postgresql.ENUM("OPTIMIST", "LASER47", name="boat")
    boat.drop(op.get_bind())

    op.drop_index(op.f("ix_user_email"), table_name="user")
    op.drop_table("user")
    # ### end Alembic commands ###
