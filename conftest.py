import datetime

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy_utils.functions import create_database, database_exists

from api.database import Base, get_db, settings
from api.models.association import Role, UserCompetition
from api.models.competition import Boat, Competition
from api.models.user import User
from main import app


@pytest.fixture
def db_engine():
    engine = create_engine(
        f"postgresql://{settings.database_username}:{settings.database_password}@{settings.database_hostname}:{settings.database_port}/competition_sailing_test"
    )
    yield engine


@pytest.fixture(scope="function")
def build_db(db_engine):
    # check if database exists
    if not database_exists(db_engine.url):
        create_database(db_engine.url)

    # drop tables
    Base.metadata.drop_all(bind=db_engine)

    # create tables
    Base.metadata.create_all(bind=db_engine)


@pytest.fixture(scope="function")
def db(db_engine):
    connection = db_engine.connect()
    connection.begin()
    db = Session(bind=connection)

    yield db

    db.rollback()
    connection.close()


@pytest.fixture
def client(db: Session):
    app.dependency_overrides[get_db] = lambda: db

    with TestClient(app) as c:
        yield c


@pytest.fixture(scope="function")
def user(db: Session):
    user = User(first_name="John", last_name="Doe", email="john.doe@gmail.com")

    db.add(user)
    db.commit()
    db.refresh(user)

    yield user


@pytest.fixture(scope="function")
def user2(db: Session):
    user = User(first_name="Jane", last_name="Doe", email="jane.doe@gmail.com")

    db.add(user)
    db.commit()
    db.refresh(user)

    yield user


@pytest.fixture(scope="function")
def user3(db: Session):
    user = User(first_name="Jake", last_name="Doe", email="jake.doe@gmail.com")

    db.add(user)
    db.commit()
    db.refresh(user)

    yield user


@pytest.fixture(scope="function")
def competition(db: Session):
    competition = Competition(
        title="European Championship",
        description="The European Championship",
        boat=Boat.OPTIMIST,
        start_date=datetime.date(year=2022, month=10, day=10),
        end_date=datetime.date(year=2022, month=11, day=10),
        location="Houston Texas",
    )

    db.add(competition)
    db.commit()
    db.refresh(competition)

    yield competition


@pytest.fixture(scope="function")
def association_organisor(db: Session, user: User, competition: Competition):
    association = UserCompetition(
        user_email=user.email,
        competition_id=competition.id,
        role=Role.ORGANISOR,
    )

    db.add(association)
    db.commit()
    db.refresh(association)

    yield association


@pytest.fixture(scope="function")
def association_official(db: Session, user2: User, competition: Competition):
    association = UserCompetition(
        user_email=user2.email,
        competition_id=competition.id,
        role=Role.OFFICIAL,
    )

    db.add(association)
    db.commit()
    db.refresh(association)

    yield association


@pytest.fixture(scope="function")
def association_competitor(db: Session, user2: User, competition: Competition):
    association = UserCompetition(
        user_email=user2.email,
        competition_id=competition.id,
        sail_country="GRE",
        sail_number=206841,
        role=Role.COMPETITOR,
    )

    db.add(association)
    db.commit()
    db.refresh(association)

    yield association
