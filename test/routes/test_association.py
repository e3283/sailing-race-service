from unittest import mock

import pytest
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from api.models import Competition, Role, User, UserCompetition

from . import association_create_params, association_update_params


@pytest.mark.parametrize("params, expected_response", association_create_params)
@mock.patch("api.services.oauth2.requests.post")
def test_create_association_params(
    mock_request,
    build_db,
    client: TestClient,
    competition: Competition,
    user: User,
    params,
    expected_response,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "john.doe@gmail.com"}}
    )

    if params.get("competition_id") == "replace":
        params["competition_id"] = str(competition.id)

    response = client.post(url="/associations/", json=params)

    assert response.status_code == expected_response["status"]
    assert response.json() == expected_response["response"]


@mock.patch("api.services.oauth2.requests.post")
def test_create_association_same_competitor_twice(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    competition: Competition,
    association_organisor: UserCompetition,
    user2: User,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "jane.doe@gmail.com"}}
    )

    response = client.post(
        url="/associations/",
        json={
            "competition_id": str(competition.id),
            "sail_country": "gre",
            "sail_number": 206841,
            "role": Role.COMPETITOR.value,
        },
    )

    assert response.status_code == 204

    association_query = db.query(UserCompetition).filter(
        UserCompetition.competition_id == competition.id
    )

    assert association_query.count() == 2

    competitor_association: UserCompetition = association_query.filter(
        UserCompetition.competition_id == competition.id,
        UserCompetition.user_email == user2.email,
    ).first()

    assert competitor_association.sail_country == "GRE"
    assert competitor_association.sail_number == 206841
    assert competitor_association.role == Role.COMPETITOR.value

    response = client.post(
        url="/associations/",
        json={
            "competition_id": str(competition.id),
            "sail_country": "gre",
            "sail_number": 206841,
            "role": Role.COMPETITOR.value,
        },
    )

    assert response.status_code == 400
    assert response.json() == {
        "detail": {
            "errors": {
                "competition_id": "not allowed to join competition more than once"
            }
        }
    }


@mock.patch("api.services.oauth2.requests.post")
def test_create_association_competitor_twice_same_sail_number(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    competition: Competition,
    association_organisor: UserCompetition,
    user2: User,
    user3: User,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "jane.doe@gmail.com"}}
    )

    response = client.post(
        url="/associations/",
        json={
            "competition_id": str(competition.id),
            "sail_country": "gre",
            "sail_number": 206841,
            "role": Role.COMPETITOR.value,
        },
    )

    assert response.status_code == 204

    association_query = db.query(UserCompetition).filter(
        UserCompetition.competition_id == competition.id
    )

    assert association_query.count() == 2

    competitor_association: UserCompetition = association_query.filter(
        UserCompetition.competition_id == competition.id,
        UserCompetition.user_email == user2.email,
    ).first()

    assert competitor_association.sail_country == "GRE"
    assert competitor_association.sail_number == 206841
    assert competitor_association.role == Role.COMPETITOR.value

    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "jake.doe@gmail.com"}}
    )

    response = client.post(
        url="/associations/",
        json={
            "competition_id": str(competition.id),
            "sail_country": "gre",
            "sail_number": 206841,
            "role": Role.COMPETITOR.value,
        },
    )

    assert response.status_code == 400
    assert response.json() == {
        "detail": {"errors": {"competition_id": "sail number is already taken"}}
    }


@mock.patch("api.services.oauth2.requests.post")
def test_create_association_same_official_twice(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    competition: Competition,
    association_organisor: UserCompetition,
    user2: User,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "jane.doe@gmail.com"}}
    )

    response = client.post(
        url="/associations/",
        json={
            "competition_id": str(competition.id),
            "role": Role.OFFICIAL.value,
        },
    )

    assert response.status_code == 204

    association_query = db.query(UserCompetition).filter(
        UserCompetition.competition_id == competition.id
    )

    assert association_query.count() == 2

    competitor_association: UserCompetition = association_query.filter(
        UserCompetition.competition_id == competition.id,
        UserCompetition.user_email == user2.email,
    ).first()

    assert not competitor_association.sail_country
    assert not competitor_association.sail_number
    assert competitor_association.role == Role.OFFICIAL.value

    response = client.post(
        url="/associations/",
        json={
            "competition_id": str(competition.id),
            "role": Role.OFFICIAL.value,
        },
    )

    assert response.status_code == 400
    assert response.json() == {
        "detail": {
            "errors": {
                "competition_id": "not allowed to join competition more than once"
            }
        }
    }


@mock.patch("api.services.oauth2.requests.post")
def test_create_successfully_association_competitor(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    competition: Competition,
    association_organisor: UserCompetition,
    user2: User,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "jane.doe@gmail.com"}}
    )

    response = client.post(
        url="/associations/",
        json={
            "competition_id": str(competition.id),
            "sail_country": "gre",
            "sail_number": 206841,
            "role": Role.COMPETITOR.value,
        },
    )

    assert response.status_code == 204

    association_query = db.query(UserCompetition).filter(
        UserCompetition.competition_id == competition.id
    )

    assert association_query.count() == 2

    competitor_association: UserCompetition = association_query.filter(
        UserCompetition.competition_id == competition.id,
        UserCompetition.user_email == user2.email,
    ).first()

    assert competitor_association.sail_country == "GRE"
    assert competitor_association.sail_number == 206841
    assert competitor_association.role == Role.COMPETITOR.value


@mock.patch("api.services.oauth2.requests.post")
def test_create_successfully_association_official(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    competition: Competition,
    association_organisor: UserCompetition,
    user2: User,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "jane.doe@gmail.com"}}
    )

    response = client.post(
        url="/associations/",
        json={
            "competition_id": str(competition.id),
            "role": Role.OFFICIAL.value,
        },
    )

    assert response.status_code == 204

    association_query = db.query(UserCompetition).filter(
        UserCompetition.competition_id == competition.id
    )

    assert association_query.count() == 2

    competitor_association: UserCompetition = association_query.filter(
        UserCompetition.competition_id == competition.id,
        UserCompetition.user_email == user2.email,
    ).first()

    assert not competitor_association.sail_country
    assert not competitor_association.sail_number
    assert competitor_association.role == Role.OFFICIAL.value


@pytest.mark.parametrize("params, expected_response", association_update_params)
@mock.patch("api.services.oauth2.requests.post")
def test_update_association_params(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    association_organisor: UserCompetition,
    association_competitor: UserCompetition,
    params,
    expected_response,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "jane.doe@gmail.com"}}
    )

    if params.get("competition_id") == "replace":
        params["competition_id"] = str(association_competitor.competition.id)

    response = client.put(url=f"/associations/{association_competitor.id}", json=params)

    assert response.status_code == expected_response["status"]
    assert response.json() == expected_response["response"]


@mock.patch("api.services.oauth2.requests.post")
def test_update_association_not_own(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    association_organisor: UserCompetition,
    association_competitor: UserCompetition,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "jane.doe@gmail.com"}}
    )

    response = client.put(
        url=f"/associations/{association_organisor.id}",
        json={
            "competition_id": str(association_organisor.competition.id),
            "role": Role.OFFICIAL.value,
        },
    )

    assert response.status_code == 403
    assert response.json() == {
        "detail": {
            "errors": {"association_id": "not allowed to access this association"}
        }
    }


@mock.patch("api.services.oauth2.requests.post")
def test_delete_association_non_existent_id(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    association_organisor: UserCompetition,
    association_competitor: UserCompetition,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "jane.doe@gmail.com"}}
    )

    response = client.delete(url="/associations/8379afe8-6640-436b-9618-19c2610b0d79")

    assert response.status_code == 404
    assert response.json() == {
        "detail": {"errors": {"association_id": "association not found"}}
    }


@mock.patch("api.services.oauth2.requests.post")
def test_delete_association(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    association_organisor: UserCompetition,
    association_competitor: UserCompetition,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "jane.doe@gmail.com"}}
    )

    response = client.delete(url=f"/associations/{association_competitor.id}")

    assert response.status_code == 204
