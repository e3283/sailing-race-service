from unittest import mock

import pytest
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from api.models import Boat, Competition, User
from api.models.association import Role, UserCompetition

from . import competition_create_params, competition_update_params


def test_get_all_competitions_empty_database(build_db, client: TestClient):
    response = client.get(url="/competitions")

    assert response.status_code == 200
    assert response.json() == []


def test_get_all_competitions(build_db, client: TestClient, competition: Competition):
    response = client.get(url="/competitions")

    assert response.status_code == 200
    assert response.json() == [
        {
            "id": str(competition.id),
            "title": competition.title,
            "description": competition.description,
            "boat": Boat.OPTIMIST.value,
            "start_date": competition.start_date.strftime("%Y-%m-%d"),
            "end_date": competition.end_date.strftime("%Y-%m-%d"),
            "location": competition.location,
            "created_at": competition.created_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
            "updated_at": competition.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        }
    ]


def test_get_competitions_by_id(build_db, client: TestClient, competition: Competition):
    response = client.get(url=f"/competitions/{competition.id}")

    assert response.status_code == 200
    assert response.json() == {
        "id": str(competition.id),
        "title": competition.title,
        "description": competition.description,
        "boat": Boat.OPTIMIST.value,
        "start_date": competition.start_date.strftime("%Y-%m-%d"),
        "end_date": competition.end_date.strftime("%Y-%m-%d"),
        "location": competition.location,
        "created_at": competition.created_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        "updated_at": competition.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
    }


def test_get_competition_by_non_existent_id(
    build_db, client: TestClient, competition: Competition
):
    response = client.get(url="/competitions/8379afe8-6640-436b-9618-19c2610b0d79")

    assert response.status_code == 404
    assert response.json() == {
        "detail": {"errors": {"competition_id": "competition not found"}}
    }


@pytest.mark.parametrize("params, expected_response", competition_create_params)
@mock.patch("api.services.oauth2.requests.post")
def test_create_competition_params(
    mock_request, build_db, client: TestClient, user: User, params, expected_response
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "john.doe@gmail.com"}}
    )

    response = client.post(url="/competitions/", json=params)

    assert response.status_code == expected_response.get("status")
    assert response.json() == expected_response.get("response")


@mock.patch("api.services.oauth2.requests.post")
def test_successful_create_competition(
    mock_request, build_db, db: Session, client: TestClient, user: User
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "john.doe@gmail.com"}}
    )

    response = client.post(
        url="/competitions/",
        json={
            "title": "European Championship",
            "description": "European Championship",
            "boat": Boat.OPTIMIST.value,
            "start_date": "2022-10-10",
            "end_date": "2022-10-15",
            "location": "Burgas, Bulgaria",
        },
    )

    assert response.status_code == 201

    competition = db.query(Competition).first()

    assert competition is not None
    assert response.json() == {
        "id": str(competition.id),
        "title": competition.title,
        "description": competition.description,
        "boat": Boat.OPTIMIST.value,
        "start_date": competition.start_date.strftime("%Y-%m-%d"),
        "end_date": competition.end_date.strftime("%Y-%m-%d"),
        "location": competition.location,
        "created_at": competition.created_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        "updated_at": competition.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
    }

    association = db.query(UserCompetition).first()

    assert association is not None
    assert association.user_email == user.email
    assert association.competition_id == competition.id
    assert association.role == Role.ORGANISOR.value


@pytest.mark.parametrize("params, expected_response", competition_update_params)
@mock.patch("api.services.oauth2.requests.post")
def test_update_competition_params(
    mock_request,
    build_db,
    client: TestClient,
    competition: Competition,
    association_organisor: UserCompetition,
    params,
    expected_response,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "john.doe@gmail.com"}}
    )

    response = client.put(url=f"/competitions/{competition.id}", json=params)

    assert response.status_code == expected_response.get("status")
    assert response.json() == expected_response.get("response")


@mock.patch("api.services.oauth2.requests.post")
def test_successful_update_competition(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    user: User,
    competition: Competition,
    association_organisor: UserCompetition,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "john.doe@gmail.com"}}
    )

    update_data = {
        "title": "World Championship",
        "description": "The World Championship",
        "boat": Boat.LASER47.value,
        "start_date": "2022-10-10",
        "end_date": "2022-10-15",
        "location": "Burgas, Bulgaria",
    }

    response = client.put(url=f"/competitions/{competition.id}", json=update_data)

    db.refresh(competition)

    assert response.status_code == 200
    assert response.json() == {
        "id": str(competition.id),
        "title": update_data["title"],
        "description": update_data["description"],
        "boat": update_data["boat"],
        "start_date": update_data["start_date"],
        "end_date": update_data["end_date"],
        "location": update_data["location"],
        "created_at": competition.created_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        "updated_at": competition.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
    }


@mock.patch("api.services.oauth2.requests.post")
def test_delete_competition_non_existent_id(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    user: User,
    competition: Competition,
    association_organisor: UserCompetition,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "john.doe@gmail.com"}}
    )

    response = client.delete(url="/competitions/8379afe8-6640-436b-9618-19c2610b0d79")

    assert response.status_code == 404


@mock.patch("api.services.oauth2.requests.post")
def test_delete_competition(
    mock_request,
    build_db,
    db: Session,
    client: TestClient,
    user: User,
    competition: Competition,
    association_organisor: UserCompetition,
):
    mock_request.return_value = mock.Mock(
        **{"status_code": 200, "json.return_value": {"sub": "john.doe@gmail.com"}}
    )

    response = client.delete(url=f"/competitions/{competition.id}")

    assert response.status_code == 204
