from api.models import Boat, Role

competition_create_params = [
    (
        {},
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "boat": "field required",
                        "description": "field required",
                        "end_date": "field required",
                        "location": "field required",
                        "start_date": "field required",
                        "title": "field required",
                    }
                }
            },
        },
    ),
    (
        {
            "title": None,
            "description": None,
            "boat": None,
            "start_date": None,
            "end_date": None,
            "location": None,
        },
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "boat": "none is not an allowed value",
                        "description": "none is not an allowed value",
                        "end_date": "none is not an allowed value",
                        "location": "none is not an allowed value",
                        "start_date": "none is not an allowed value",
                        "title": "none is not an allowed value",
                    }
                }
            },
        },
    ),
    (
        {
            "title": "Euro Championship",
            "description": "Euro Championship",
            "boat": Boat.OPTIMIST.value,
            "start_date": "2022-10-10",
            "end_date": "2022-8-10",
            "location": "Athens",
        },
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {"start_date": "start date cannot be later than end date"}
                }
            },
        },
    ),
    (
        {
            "title": "Euro Championship",
            "description": "Euro Championship",
            "boat": "invalidboat",
            "start_date": "2022-10-10",
            "end_date": "2022-11-10",
            "location": "Athens",
        },
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "boat": "value is not a valid enumeration member; permitted: 'OPTIMIST', 'LASER47'"
                    }
                }
            },
        },
    ),
]

competition_update_params = competition_create_params

association_create_params = [
    (
        {},
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "competition_id": "field required",
                        "role": "field required",
                    }
                }
            },
        },
    ),
    (
        {
            "competition_id": None,
            "role": None,
        },
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "competition_id": "none is not an allowed value",
                        "role": "none is not an allowed value",
                    }
                }
            },
        },
    ),
    (
        {
            "competition_id": "replace",
            "role": "invalid",
        },
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "role": "value is not a valid enumeration member; permitted: 'ORGANISOR', 'OFFICIAL', 'COMPETITOR'",
                    }
                }
            },
        },
    ),
    (
        {
            "competition_id": "replace",
            "role": Role.ORGANISOR.value,
        },
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "role": "value is not a valid enumeration member; permitted: 'COMPETITOR', 'OFFICIAL'"
                    }
                }
            },
        },
    ),
    (
        {
            "competition_id": "replace",
            "role": Role.COMPETITOR.value,
        },
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "sail": "provide a sail country and number as a competitor"
                    }
                }
            },
        },
    ),
    (
        {
            "competition_id": "replace",
            "role": Role.OFFICIAL.value,
            "sail_country": "GRE",
            "sail_number": 206841,
        },
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "sail": "not allowed to provide a sail country and number as an official"
                    }
                }
            },
        },
    ),
]

association_update_params = association_create_params
