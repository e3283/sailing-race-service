from typing import List, Optional

from fastapi import APIRouter, Depends, HTTPException, Response, Security, status
from pydantic import UUID4
from sqlalchemy.orm import Session

from api import get_db
from api.models import Competition, Role, User, UserCompetition
from api.schemas import (
    CompetitionCreate,
    CompetitionOut,
    CompetitionUpdate,
    CompetitionUserOut,
)
from api.services.oauth2 import get_current_user

router = APIRouter(prefix="/competitions", tags=["Competitions"])


@router.get("/", status_code=status.HTTP_200_OK, response_model=List[CompetitionOut])
async def index(
    limit: int = 10,
    offset: int = 0,
    title: Optional[str] = "",
    db: Session = Depends(get_db),
):
    """Handle returning all competitions."""

    competitions = (
        db.query(Competition)
        # we are only intrested in future competitions & filter by the title of the competition
        # .filter(
        #     Competition.start_date >= datetime.datetime.now(tz=pytz.utc),
        #     Competition.title.ilike(f"%{title}%"),
        # )
        # by specifying the limit and offset we can do server side pagination
        .limit(limit).offset(offset)
        # return all entries that meet the criteria
        .all()
    )

    return competitions


@router.get("/{id}", status_code=status.HTTP_200_OK, response_model=CompetitionOut)
async def get_by_id(
    id: UUID4,
    db: Session = Depends(get_db),
):
    """Handle returning one competition by id."""

    # get the competition form the database by the provided id
    competition = db.query(Competition).get(id)

    # raise an erro when a competition is not found
    if not competition:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={"errors": {"competition_id": "competition not found"}},
        )

    return competition


@router.post("/", status_code=status.HTTP_201_CREATED, response_model=CompetitionOut)
async def create(
    competition: CompetitionCreate,
    current_user: User = Security(get_current_user, scopes=["competitions:write"]),
    db: Session = Depends(get_db),
):
    """Handle creating a competition."""

    # create a new competition model based on the provided schema data
    new_competition = Competition(**competition.dict())

    # save it to the database
    db.add(new_competition)
    db.commit()

    # refresh the new competition to get the updated data
    db.refresh(new_competition)

    # establish user (organisor) - competition relationship
    new_user_competition = UserCompetition(
        user_email=current_user.email,
        competition_id=new_competition.id,
        role=Role.ORGANISOR,
    )

    db.add(new_user_competition)
    db.commit()

    return new_competition


@router.put("/{id}", status_code=status.HTTP_200_OK, response_model=CompetitionOut)
async def update(
    id: UUID4,
    updated_competition: CompetitionUpdate,
    current_user: User = Security(get_current_user, scopes=["competitions:update"]),
    db: Session = Depends(get_db),
):
    """Handle updating a competition."""

    # get the competition form the database by the provided id
    competition_query = db.query(Competition).filter(Competition.id == id)
    competition: Competition = competition_query.first()

    # raise an erro when a competition is not found
    if not competition:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={"errors": {"competition_id": "competition not found"}},
        )

    if not competition.user_roles.filter(
        UserCompetition.user_email == current_user.email,
        UserCompetition.role == Role.ORGANISOR,
    ).first():
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail={"errors": {"role": "must be organizor to update competition"}},
        )

    # update the competition with the new provided data
    updated_competition = competition_query.update(
        updated_competition.dict(), synchronize_session=False
    )
    db.commit()

    return competition_query.first()


@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete(
    id: UUID4,
    current_user: User = Security(get_current_user, scopes=["competitions:delete"]),
    db: Session = Depends(get_db),
):
    """Handle deleting a competition."""

    # get the competition form the database by the provided id
    competition = db.query(Competition).filter(Competition.id == id).first()

    # raise an erro when a competition is not found
    if not competition:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={"errors": {"competition_id": "competition not found"}},
        )

    if not competition.user_roles.filter(
        UserCompetition.user_email == current_user.email,
        UserCompetition.role == Role.ORGANISOR,
    ).first():
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail={"errors": {"role": "must be organisor to delete competition"}},
        )

    db.delete(competition)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get(
    "/{competition_id}/associations",
    status_code=status.HTTP_200_OK,
    response_model=List[CompetitionUserOut],
)
async def get_user_competitions(
    competition_id: UUID4,
    current_user: User = Security(get_current_user, scopes=["competitions:read"]),
    db: Session = Depends(get_db),
):
    """Handle returning all competitors of a competition."""

    associations_query = db.query(UserCompetition).filter(
        UserCompetition.competition_id == competition_id
    )

    if not associations_query.all():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={"errors": {"competition_id": "competition not found"}},
        )

    associations = associations_query.filter(
        UserCompetition.role == Role.COMPETITOR
    ).all()

    return associations
