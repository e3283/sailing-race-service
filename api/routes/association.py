from fastapi import APIRouter, Depends, HTTPException, Response, Security, status
from pydantic import UUID4
from sqlalchemy.orm import Session

from api import get_db
from api.models import Competition, User, UserCompetition
from api.models.association import Role
from api.schemas import AssociationCreate, AssociationUpdate
from api.services import get_current_user

router = APIRouter(prefix="/associations", tags=["Associations"])


@router.post("/", status_code=status.HTTP_204_NO_CONTENT)
async def create(
    association: AssociationCreate,
    current_user: User = Security(get_current_user, scopes=["associations:write"]),
    db: Session = Depends(get_db),
):
    """Handle adding a user to a competition."""

    association_query = db.query(UserCompetition).filter(
        UserCompetition.competition_id == association.competition_id
    )

    if not db.query(Competition).get(association.competition_id):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={"errors": {"competition_id": "competition not found"}},
        )

    # a user can only join a competition once
    if association_query.filter(
        UserCompetition.user_email == current_user.email
    ).first():
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail={
                "errors": {
                    "competition_id": "not allowed to join competition more than once"
                }
            },
        )

    if association.role == Role.COMPETITOR.value:
        if association_query.filter(
            UserCompetition.sail_number == association.sail_number
        ).first():
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail={"errors": {"competition_id": "sail number is already taken"}},
            )

    new_association = UserCompetition(**association.dict())
    new_association.user_email = current_user.email

    db.add(new_association)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.put("/{id}", status_code=status.HTTP_204_NO_CONTENT)
async def update(
    id: UUID4,
    updated_association: AssociationUpdate,
    current_user: User = Security(get_current_user, scopes=["associations:update"]),
    db: Session = Depends(get_db),
):
    """Handle updating a user associated to a competition."""

    association_query = db.query(UserCompetition).filter(UserCompetition.id == id)
    association: UserCompetition = association_query.first()

    if not association:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={"errors": {"association_id": "association not found"}},
        )

    if association.user_email != current_user.email:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail={
                "errors": {"association_id": "not allowed to access this association"}
            },
        )

    # a user can only join a competition once
    if association_query.filter(
        UserCompetition.user_email == current_user.email
    ).first():
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail={
                "errors": {
                    "competition_id": "not allowed to join competition more than once"
                }
            },
        )

    if association.role == Role.COMPETITOR.value:
        if association_query.filter(
            UserCompetition.sail_number == association.sail_number
        ).first():
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail={"errors": {"competition_id": "sail number is already taken"}},
            )

    updated_association = association_query.update(
        updated_association.dict(), synchronize_session=False
    )
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.delete(
    "/{id}",
    status_code=status.HTTP_204_NO_CONTENT,
)
async def delete(
    id: UUID4,
    current_user: User = Security(get_current_user, scopes=["associations:delete"]),
    db: Session = Depends(get_db),
):
    """Handle adding a user to a competition."""

    user_competition_query = db.query(UserCompetition).filter(UserCompetition.id == id)
    user_competition = user_competition_query.first()

    if not user_competition:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={"errors": {"association_id": "association not found"}},
        )

    if user_competition.user_email != current_user.email:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail={
                "errors": {"association_id": "not allowed to access this association"}
            },
        )

    user_competition_query.delete()
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)
