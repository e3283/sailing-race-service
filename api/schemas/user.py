import datetime

from pydantic import UUID4, BaseModel, EmailStr


class UserBase(BaseModel):
    """Represent a user that is the base for all other as a schema."""

    first_name: str
    last_name: str
    email: EmailStr

    class Config:
        orm_mode = True


class UserOut(UserBase):
    """Represent a user that is returned to the user as a schema."""

    id: UUID4

    created_at: datetime.datetime
    updated_at: datetime.datetime


class UserAssociation(BaseModel):

    id: UUID4
    first_name: str
    last_name: str

    class Config:
        orm_mode = True
