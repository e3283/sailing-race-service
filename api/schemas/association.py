from typing import Optional

from fastapi import HTTPException, status
from pydantic import UUID4, BaseModel, constr, root_validator

from api.models import Role


class AssociationBase(BaseModel):
    """Represent a sailing user - competition that is the base for all other as a schema."""

    competition_id: UUID4  # the unique id of the competition

    sail_country: Optional[
        constr(strip_whitespace=True, min_length=3, max_length=3)
    ] = None
    sail_number: Optional[int] = None

    role: Role

    # provide cofiguration for model schema behaviour
    class Config:
        orm_mode = True
        use_enum_values = True


class AssociationCreate(AssociationBase):
    """Represent a sailing user - competition that is sent from the user to create as a schema."""

    @root_validator
    def root_validation(cls, values):
        """Some validation is happening."""

        # get the sail_country, sail_number & role
        sail_country: str = values.get("sail_country")
        sail_number: int = values.get("sail_number")
        role: Role = values.get("role")

        if role == Role.ORGANISOR.value:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail={
                    "errors": {
                        "role": "value is not a valid enumeration member; permitted: 'COMPETITOR', 'OFFICIAL'"
                    }
                },
            )

        # a competitor must provide a sail_country & sail_number
        if role == Role.COMPETITOR.value:
            if not (sail_country or sail_number):
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail={
                        "errors": {
                            "sail": "provide a sail country and number as a competitor"
                        }
                    },
                )

        # an official must not provide a sail_country & sail_number
        if role == Role.OFFICIAL.value:
            if sail_country or sail_number:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail={
                        "errors": {
                            "sail": "not allowed to provide a sail country and number as an official"
                        }
                    },
                )

        if sail_country:
            values["sail_country"] = sail_country.upper()

        return values


class AssociationUpdate(AssociationCreate):
    """Represent a sailing user - competition that is sent from the user to update as a schema."""

    pass
