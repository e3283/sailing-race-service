import datetime
from typing import Optional

from fastapi import HTTPException, status
from pydantic import UUID4, BaseModel, root_validator

from api.models import Boat
from api.schemas.user import UserAssociation


class CompetitionBase(BaseModel):
    """Represent a sailing competition that is the base for all other as a schema."""

    title: str  # title of the competition
    description: str  # description of the competition
    boat: Boat  # the type of boat of the competition
    start_date: datetime.date  # the start date of the competition
    end_date: datetime.date  # the end date of the competition

    location: str  # the location of the competition

    # provide cofiguration for model schema behaviour
    class Config:
        orm_mode = True
        use_enum_values = True


class CompetitionOut(CompetitionBase):
    """Represent a sailing competition that is returned to the user as a schema."""

    id: UUID4  # the unique id of the competition

    created_at: datetime.datetime  # the creation date of the competition
    updated_at: datetime.datetime  # the latest update date of the competition


class CompetitionCreate(CompetitionBase):
    """Represent a sailing competition that is sent from the user to create as a schema."""

    @root_validator
    def root_validation(cls, values):
        """Some validation is happening."""

        start_date: datetime.date = values.get("start_date")
        end_date: datetime.date = values.get("end_date")
        if start_date and end_date:
            if start_date > end_date:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail={
                        "errors": {
                            "start_date": "start date cannot be later than end date"
                        }
                    },
                )

        return values


class CompetitionUpdate(CompetitionCreate):
    """Represent a sailing competition that is sent from the user to update as a schema."""

    pass


class CompetitionUserOut(BaseModel):

    id: UUID4  # the unique id of the competition

    user: UserAssociation

    sail_country: Optional[str] = None
    sail_numberr: Optional[int] = None

    created_at: datetime.datetime  # the creation date of the competition
    updated_at: datetime.datetime  # the latest update date of the competition

    class Config:
        orm_mode = True
        use_enum_values = True
