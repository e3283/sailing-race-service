import sqlalchemy as sa

from api.database import Base


class User(Base):
    """Representing a user as a database table."""

    first_name = sa.Column(sa.Text, nullable=False)
    last_name = sa.Column(sa.Text, nullable=False)
    email = sa.Column(sa.Text, nullable=False, unique=True, index=True)

    # set the relationship with the UserCompetition table
    competition_roles = sa.orm.relationship(
        "UserCompetition",
        back_populates="user",
        lazy="dynamic",
    )
