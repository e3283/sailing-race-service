from api.database import Base
from api.models.association import Role, UserCompetition
from api.models.competition import Boat, Competition
from api.models.user import User
