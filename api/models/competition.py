from enum import Enum

import sqlalchemy as sa

from api.database import Base


class Boat(str, Enum):

    OPTIMIST = "OPTIMIST"
    LASER47 = "LASER47"

    def __repr__(self) -> str:
        return self.name.value


class Competition(Base):
    """Represent a sailing competion as a database table."""

    title = sa.Column(sa.Text, nullable=False)  # title of the competition
    description = sa.Column(sa.Text, nullable=False)  # description of the competition
    boat = sa.Column(
        sa.Enum(Boat), nullable=False
    )  # the type of boat that will be reaced in the competition
    start_date = sa.Column(sa.Date, nullable=False)  # the start date of the competition
    end_date = sa.Column(sa.Date, nullable=False)  # the end date of the competition

    location = sa.Column(sa.Text, nullable=False)  # the location of the competition

    # set the relationship with the UserCompetition table
    user_roles = sa.orm.relationship(
        "UserCompetition",
        back_populates="competition",
        cascade="all, delete",
        lazy="dynamic",
    )
