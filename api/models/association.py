from enum import Enum

import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

from api.database import Base


class Role(str, Enum):

    ORGANISOR = "ORGANISOR"
    OFFICIAL = "OFFICIAL"
    COMPETITOR = "COMPETITOR"

    def __repr__(self) -> str:
        return self.name


class UserCompetition(Base):
    """Represent the association between a user and competition."""

    user_email = sa.Column(
        sa.Text, sa.ForeignKey("user.email", ondelete="SET NULL"), nullable=True
    )  # store the account email
    competition_id = sa.Column(
        UUID(as_uuid=True), sa.ForeignKey("competition.id"), nullable=False
    )  # store the competition id

    # a competitor has a unique sail number that comprises of a country code (len:3) and a unique sailing number
    sail_country = sa.Column(sa.String(length=3), nullable=True)
    sail_number = sa.Column(sa.BigInteger, nullable=True)

    role = sa.Column(sa.Enum(Role), nullable=False)

    # define the unique constraints for this table
    __table_args__ = (
        sa.UniqueConstraint(user_email, competition_id),
        sa.UniqueConstraint(competition_id, sail_country, sail_number),
    )

    # set the relationship with the User class
    user = sa.orm.relationship("User", back_populates="competition_roles")

    # set the relationship with the Competition class
    competition = sa.orm.relationship("Competition", back_populates="user_roles")
