from pydantic import BaseSettings


class Settings(BaseSettings):
    # app settings
    auth_service: str
    fastapi_app: str
    fastapi_port: int

    # database settings
    database_hostname: str
    database_port: str
    database_password: str
    database_name: str
    database_username: str

    # broker settings
    broker_hostname: str
    broker_port: str
    broker_password: str
    broker_vhost: str
    broker_username: str

    broker_exchange: str
    broker_queue: str
    broker_route: str

    class Config:
        env_file = ".env"


settings = Settings()
