import json

from aio_pika import ExchangeType, connect_robust
from aio_pika.abc import AbstractIncomingMessage
from sqlalchemy.orm import Session

from api.config import settings
from api.database import SessionLocal
from api.models.user import User
from api.schemas.user import UserBase

db: Session = SessionLocal()


async def consume(loop):
    connection = await connect_robust(
        f"amqp://{settings.broker_username}:{settings.broker_password}@{settings.broker_hostname}/{settings.broker_vhost}",
        loop=loop,
    )

    async with connection:
        channel = await connection.channel()
        exchange = await channel.declare_exchange(
            name=settings.broker_exchange, type=ExchangeType.DIRECT
        )

        queue = await channel.declare_queue(name=settings.broker_queue)

        await queue.bind(exchange=exchange, routing_key="user.register")
        await queue.bind(exchange=exchange, routing_key="user.update")
        await queue.bind(exchange=exchange, routing_key="user.delete")

        async with queue.iterator() as iterator:
            message: AbstractIncomingMessage
            async for message in iterator:
                async with message.process():

                    print(message.routing_key)

                    # register a new user
                    if message.routing_key == "user.register":
                        user = UserBase(**json.loads(message.body.decode()))

                        # in case of a message comming through twice do not add it
                        if not db.query(User).filter(User.email == user.email).first():
                            new_user = User(**user.dict())
                            db.add(new_user)
                            db.commit()

                    if message.routing_key == "user.update":
                        user = UserBase(**json.loads(message.body.decode()))
                        db.query(User).filter(User.email == user.email).update(
                            user.dict(), synchronize_session=False
                        )
                        db.commit()

                    if message.routing_key == "user.delete":
                        user = json.loads(message.body.decode())
                        db.query(User).filter(User.email == user["email"]).delete()
                        db.commit()

        return connection
