import requests
from fastapi import Depends, HTTPException, Request, status
from fastapi.security import SecurityScopes
from sqlalchemy.orm import Session

from api.config import settings
from api.database import get_db
from api.models import User


async def get_current_user(
    security_scopes: SecurityScopes,
    request: Request,
    db: Session = Depends(get_db),
):
    """Utility function that returns the user data of the user that is making the request."""
    try:
        response = requests.post(
            url=f"{settings.auth_service}/auth/verify?required_scopes={security_scopes.scope_str}",
            headers={"Authorization": request.headers.get("authorization")},
        )

        # raise exception if unauthorized
        response.raise_for_status()
    except requests.HTTPError as error:
        raise HTTPException(
            status_code=error.response.status_code,
            detail=error.response.json().get("detail"),
        )

    sub = response.json().get("sub")

    if not (user := db.query(User).filter(User.email == sub).first()):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    return user
