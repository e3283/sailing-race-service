import asyncio
import logging

from fastapi import FastAPI, Request, status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse

from api.config import settings
from api.consumer import consume
from api.database import Base, get_db

app = FastAPI()


def create_app():
    """Intialize the application."""

    # whitelist all CORS
    origins = ["*"]

    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    # register the api endpoints

    # competition endpoint
    from api.routes import competition

    app.include_router(competition.router)

    # association endpoint
    from api.routes import association

    app.include_router(association.router)

    @app.exception_handler(RequestValidationError)
    async def validation_exception_handler(
        request: Request, exc: RequestValidationError
    ):
        errors = dict()
        for error in exc.errors():
            errors[error["loc"][-1]] = error["msg"]

        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content=jsonable_encoder({"detail": {"errors": errors}}),
        )

    @app.get("/health-check", status_code=status.HTTP_200_OK)
    def health_check():
        return {"healthy": True}

    @app.on_event("startup")
    async def startup():
        loop = asyncio.get_event_loop()
        asyncio.ensure_future(consume(loop))

    return app
