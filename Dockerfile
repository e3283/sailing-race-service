# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.9.5

WORKDIR /app
RUN pip install --no-cache-dir pipenv
COPY . .

ENV PIPENV_VENV_IN_PROJECT=1
RUN pipenv sync

EXPOSE 8000
# run the main.py
CMD ["pipenv", "run", "python", "main.py"]